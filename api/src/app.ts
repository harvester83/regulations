import express from 'express';
import { Request, Response } from 'express';
import { domain } from './domain/domain';
import { pool } from './db';

const app = express();
const router = express.Router();
app.use('/api', router);

router.get('/articles', async (req: Request, res: Response) => {
  res.setHeader('Access-Control-Allow-Origin', domain);
  let data = await pool.query('SELECT * FROM documents');

  res.send(data.rows);
});

router.get('/articles/search', async (req: Request, res: Response) => {
  res.setHeader('Access-Control-Allow-Origin', domain);
  let data: any =  '';

  if (req.query.category.length) {
    data = await pool.query(
      `SELECT * FROM documents WHERE category='${req.query.category}' and title ILIKE '%${req.query.title}%';`
    );
  } else {
    data = await pool.query(
      `SELECT * FROM documents WHERE title ILIKE '%${req.query.title}%';`
    );
  }

  res.send(data.rows);
});

app.listen(5000, () => {
  console.log('server connection!');
});
