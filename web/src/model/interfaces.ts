export interface IData {
  id: number,
  title: string,
  category: string,
  text: string,
  link: string,
}

export interface ICustomizedInputBase {
  setData: (value: IData[]) => void;
  setTitle: (value: string) => void;
  category: string
}