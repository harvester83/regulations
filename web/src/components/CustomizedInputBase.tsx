import React, { useState } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import { url } from '../url/config';
import { ICustomizedInputBase } from '../model/interfaces';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      width: 400,
      marginBottom: 10,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  })
);

export default function CustomizedInputBase(props: ICustomizedInputBase) {
  const { setData, setTitle, category } = props;


  const handleChangeSearchInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    let title = event.target.value;
    setTitle(title);

    fetch(url + '/articles/search?title' + '=' + title +'&category=' + category)
      .then(result => result.json())
      .then(data => {
        setTimeout(() => {
          setData(data);
        }, 1000);

      });
  };

  const classes = useStyles();

  return (
    <Paper
      component="form"
      className={classes.root}
    >
      <InputBase
        className={classes.input}
        placeholder="Axtar"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleChangeSearchInput(event)}
      />
    </Paper>
  );
}
