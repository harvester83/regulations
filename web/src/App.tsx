import React, { useEffect, useState } from 'react';
import './App.css';
import { url } from './url/config';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { FormControl, Grid, InputLabel, MenuItem, Select } from '@material-ui/core';
import CustomizedInputBase from './components/CustomizedInputBase';
import { IData } from './model/interfaces';
import Viewer, { Worker } from '@phuocng/react-pdf-viewer';
import '@phuocng/react-pdf-viewer/cjs/react-pdf-viewer.css';
import { Link, Visibility } from '@material-ui/icons';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const App: React.FC = () => {
  const [data, setData] = useState<IData[]>([]);

  const [title, setTitle] = useState<string>('');

  useEffect(() => {
    fetch(url + '/articles')
      .then(response => {
        return response.json();
      })
      .then(data => {
        setData(data);
      });
  }, []);

  const [category, setCategory] = React.useState('');
  const changeCategory = (event: React.ChangeEvent<{ value: unknown }>) => {
    let category = event.target.value;
    setCategory(category as string);

    fetch(url + '/articles/search?title' + '=' + title +'&category=' + category)
      .then(result => result.json())
      .then(data => {
        setData(data);
      });
  };

  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      formControl: {
        marginBottom: theme.spacing(1),
        width: 400,
      },

      selectEmpty: {
        marginTop: theme.spacing(2),
      },
      table: {
        minWidth: 650,
      },

      tablecell: {
        fontWeight: 600,
      },

      modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
    })
  );
  const classes = useStyles();

  return (
    <div className="App">
      <h1 style={{ textAlign: 'center' }}>Regulations</h1>

      <Grid>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel id="demo-simple-select-outlined-label">Category</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={category}
            onChange={changeCategory}
            label="Category"
          >
            {data.map(row => (
              <MenuItem value={row.category}>{row.category}</MenuItem>
            ))}
          </Select>
        </FormControl>

        <CustomizedInputBase setData={setData} setTitle={setTitle} category={category}/>

        <TableContainer component={Paper}>
          <Table className={classes.table} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell className={classes.tablecell} align="left">
                  Title
                </TableCell>
                <TableCell className={classes.tablecell} align="left">
                  Category
                </TableCell>
                <TableCell className={classes.tablecell} align="center">
                  Link
                </TableCell>
                <TableCell className={classes.tablecell} align="center">
                  Preview
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.length > 0 &&
              data.map(row => (
                <TableRow key={row.id}>
                  <TableCell align="left" component="th" scope="row">
                    <a href={row.link}>{row.title}</a>
                  </TableCell>

                  <TableCell align="left">{row.category}</TableCell>

                  <TableCell align="center">
                    <a href={row.link} target="_blank">
                      <Link />
                    </a>
                  </TableCell>

                  <TableCell align="center" onClick={handleOpen}>
                    <Visibility />

                    <Modal
                      aria-labelledby="transition-modal-title"
                      aria-describedby="transition-modal-description"
                      className={classes.modal}
                      open={open}
                      onClose={handleClose}
                      closeAfterTransition
                      BackdropComponent={Backdrop}
                      BackdropProps={{
                        timeout: 500,
                      }}
                    >
                      <Fade in={open}>
                        <div
                          style={{
                            height: '650px',
                            marginTop: '100px',
                          }}
                        >
                          <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.2.228/build/pdf.worker.min.js">
                            <Viewer fileUrl="./test.pdf"/>
                          </Worker>
                        </div>
                      </Fade>
                    </Modal>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </div>
  );
};

export default App;
