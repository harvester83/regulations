CREATE DATABASE regulations;

DROP TABLE documents;

CREATE TABLE documents(
   id serial PRIMARY KEY,
   title VARCHAR (355) NULL,
   category VARCHAR (355) NOT NULL,
   text VARCHAR (355)  NULL,
   link VARCHAR (355)  NULL
);

INSERT INTO documents (id, title, category, text, link) VALUES
(1, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(2, 'Инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(3, 'Рроектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(4, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(5, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(6, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(7, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(8, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(9, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod'),
(10, 'Временная инструкция по проектированию сооружений для очистки поверхностных сточных вод', 'Mühəndis sistemləri', '', 'https://arxkom.gov.az/qanunvericilik/normativler/muhendis-sistemleri/vremennaya-instruktsiya-po-proektirovaniyu-sooruzheniy-dlya-ochistki-poverkhnostnykh-stochnykh-vod');
